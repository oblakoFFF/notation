# Реализация обратной польской записи <Собинников Павел 603Б1>

### **Класс:** _Notation_
#### Доступные методы:
    Notation(String _infix);
    String toPostfix();
    Double toResult();
    
### Точки роста:
    Добавить Exception в класс InfixNotation, и реализовать математические функци(sin(), cos(), ...)
    Реализация класса PostfixNotation + Exception