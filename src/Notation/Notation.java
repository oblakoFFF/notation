package Notation;

import java.util.Stack;

public final class Notation {

    private Notation() {}

    private static int isPriority(Character ch){
        if (ch == '^') return 4;
        if (ch == '*' || ch == '/') return 3;
        if (ch == '+' || ch == '-') return 2;
        if (ch == '(' || ch == ')') return 1;
        if (ch == ' ') return -1;
        return 0;
    }

    public static String toPostfix(String _infix){
        Stack<Character> stack = new Stack<>();
        StringBuilder _postfix = new StringBuilder(0);
        for (Character ch : _infix.toCharArray()) {
            if (isPriority(ch) == 0) {
                _postfix.append(ch);
            }
            else {
                if (isPriority(ch) != 1) {
                    if (_postfix.length() > 0 && _postfix.toString().charAt(_postfix.length()-1) != ' ') _postfix.append(' ');
                    while(!stack.isEmpty() && isPriority(ch) <= isPriority(stack.lastElement())) _postfix.append(stack.pop()).append(" ");
                }
                switch (isPriority(ch)){
                    case 4:{
                        stack.push(ch);
                        break;
                    }
                    case 3:{
                        stack.push(ch);
                        break;
                    }
                    case 2:{
                        stack.push(ch);
                        break;
                    }
                    case 1:{
                        if (ch == ')') {
                            while(stack.lastElement() != '(') { _postfix.append(" ").append(stack.pop()); }
                            stack.pop();
                        }
                        else stack.push(ch);
                        break;
                    }
                }
            }
        }
        while(!stack.isEmpty()){
            _postfix.append(" ").append(stack.pop());
        }
        return _postfix.toString();
    }

    public static Double toResult(String _infix){
        Stack<Double> stack = new Stack<>();
        stack.push(0.0);
        double result = 0.0;
        boolean isInt = true;
        boolean space = false;
        int numDouble = 1;

        String _postfix = toPostfix(_infix);

        for (Character ch : _postfix.toCharArray()){
            switch (isPriority(ch)){
                case 4:{
                    result = Math.pow(stack.pop(),result);
                    break;
                }
                case 3:{
                    if (ch == '*') result=stack.pop()*result;
                    if (ch == '/') result=stack.pop()/result;
                    break;
                }
                case 2:{
                    if (ch == '+') result=stack.pop()+result;
                    if (ch == '-') result=stack.pop()-result;
                    break;
                }
                case -1:{
                    isInt = true;
                    numDouble = 1;
                    space = true;
                    break;
                }
                default:{
                    if (space){
                        stack.push(result);
                        result = (double)Character.digit(ch,10);
                        space = false;
                        break;
                    }
                    if (ch == '.' || ch == ',') {
                        isInt = false;
                    }
                    else{
                        if (isInt){
                            if (result != 0.0) result*=10;
                            result = result + (double)Character.digit(ch,10);
                        }
                        else{
                            result+=(double)ch/Math.pow(10, numDouble++);
                        }
                    }
                }
            }
        }
        return result;
    }
}
